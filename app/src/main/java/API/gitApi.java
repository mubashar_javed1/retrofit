package API;

import model.DataModel;
import retrofit.Call;
import retrofit.http.GET;

/*
 * Created by metikulousmacmini on 1/25/16.
 */
public interface gitApi {
    @GET("/")
    //public void getData(Callback<DataModel> response);
    Call<DataModel> getTasks();
}
