package com.example.metikulousmacmini.dispalydata;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import API.gitApi;
import model.DataModel;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;



public class MainActivity extends AppCompatActivity {

    TextView name, age,sex, email, startDate;

    public static final String BASE_URL = "http://192.168.2.102:3000/student/get";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView)findViewById(R.id.name);
        age = (TextView)findViewById(R.id.age);
        sex = (TextView)findViewById(R.id.sex);
        email = (TextView)findViewById(R.id.email);
        startDate = (TextView)findViewById(R.id.date);

        Button getDate = (Button)findViewById(R.id.get_data);

        getDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final ProgressDialog dialog = new ProgressDialog(getApplicationContext());
                dialog.setMessage("Please Wait");

                Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
                gitApi git = retrofit.create(gitApi.class);


                Call<DataModel> call = git.getTasks();
               /* call.enqueue(new Callback<DataModel>() {

                    @Override
                    public void onResponse(Response<DataModel> response, Retrofit retrofit) {
                       // Log.d("yes", " Throwable is " +response.toString() + " " + retrofit);

                    }

                    @Override
                    public void onFailure(Throwable t) {

                        Log.d("CallBack", " Throwable is " +t);
                    }
                });*/
                try {
                    DataModel model = call.execute().body();
                    name.setText(model.getName());
                    age.setText(model.getAge());
                    sex.setText(model.getSex());
                    email.setText(model.getEmail());
                    startDate.setText(model.getJoindate());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Defining the method
              /*  git.getTasks() {
                    @Override
                    public void onResponse(Response<DataModel> response, Retrofit retrofit) {
                        Log.d("Data", response.toString());
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }

                });*/

            }
        });
    }

}
